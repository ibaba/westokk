import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom'
import Home from './pages/Home';
import Login from './pages/Login';

ReactDOM.render(<BrowserRouter>
    <div>
        <Route exact path='/' component={Home} />

        <Route path='/login' component={Login} />
        {/* <Route path='/register' component={Register} />
    <Route path='/profile' component={Profile} />
    <Route path='/Apps' component={Apps} />
    <Route path='/application/design/:appId?' component={Design} />
    <Route path='/application/rubriques/:appId' component={Rubriques} />
    <Route path='/application/notifications/:appId' component={Notifications} /> */}
    </div>
</BrowserRouter>,
    document.getElementById('root'));

