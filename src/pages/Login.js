import React, { Component } from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  TextField,
  CardHeader,
  IconButton,
  CssBaseline,
  Avatar,
  CircularProgress,
  LinearProgress,
  Icon,
  Divider
} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/StarBorder";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { withRouter, Link, Redirect } from 'react-router-dom';
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Slide from "@material-ui/core/Slide";
import Grow from "@material-ui/core/Grow";
import { Row, Col, Image } from "react-bootstrap";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";

import { DropzoneDialog } from "material-ui-dropzone";
import { fontFamily } from "@material-ui/system";

//import bg00 from '../assets/bgOO.jpg';

export default class Login extends Component {
  appBar() {
    return (
      <Grid item xs={12} md={12}>
        <Row>
          <Col>
            <Image
              src={
                "https://trello-attachments.s3.amazonaws.com/5daaf63d70ab311cffed9a03/5dab01c34d6a05454eb475cd/c08bf0cfc4d99495ccbdc2a611b440cd/We.svg"
              }
              style={{ width: 80, height: "auto" }}
            />
          </Col>
          <div
            style={{
              justifyContent: "center",
              color: "grey",
              fontSize: 16,
              marginLeft: 20
            }}
          >
            Contests
          </div>
          <div
            style={{
              justifyContent: "center",
              color: "grey",
              fontSize: 16,
              marginLeft: 20
            }}
          >
            Upgrade
          </div>
          <div
            style={{
              justifyContent: "center",
              color: "grey",
              fontSize: 16,
              marginLeft: 20
            }}
          >
            Help
          </div>
          <Link to={{
            pathname: "/login",
            //  data: 'data' // your data array of objects
          }}>
            <div
              style={{
                justifyContent: "center",
                color: "#61c8d0",
                fontSize: 16,
                marginLeft: 20
              }}
            >
              Sign in
          </div>
          </Link>
        </Row>
      </Grid>
    );
  }

  registerForm() {
    return (
      <Grid item xs={12} md={12} style={{ padding: 10 }}>
        <Row style={{ justifyContent: "center", alignContent: "center" }}>
          <div style={{ height: 1, width: 100, backgroundColor: "lightgrey", marginTop: 30 }} />
          <Col>
            <div style={{ justifyContent: "center", color: "#61C8CF", fontSize: 30, fontWeight: "bold" }}>
              Register.
              </div>
            <div>
              Let's get you set up.
              </div>
          </Col>
        </Row>

        <div
          style={{
            // backgroundColor: "#F8F9FB",
            // padding: 10,
            marginTop: 30,
            borderRadius: 5,
            marginRight: 100,
            marginLeft: 100

          }}
        >

          <TextField
            // id="standard-name"
            label="Full name"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            margin="normal"
            fullWidth
          />
          <TextField
            // id="standard-name"
            label="Your email"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            margin="normal"
            fullWidth
          />
          <TextField
            // id="standard-name"
            label="Password"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            type="password"
            margin="normal"
            fullWidth
          />
          <TextField
            // id="standard-name"
            label="Re-enter password"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            type="password"
            margin="normal"
            fullWidth
          />
          <div style={{ height: 30 }} />
        </div>
        <Button
          type="submit"
          style={{
            borderRadius: 5,
            backgroundColor: "#243D51",
            marginTop: -20,
            //marginLeft: 10,
            alignSelf: "flex_right",
            flex: 1,
            width: 130
          }}
        >
          <div style={{ color: "white", fontSize: 16, textTransform: "none" }}>
            Transfer
          </div>
        </Button>
      </Grid>
    );
  }

  loginForm() {
    return (
      <Grid item xs={12} md={12} style={{ marginTop: 100 }}>
        <Row style={{ justifyContent: "center", alignContent: "center" }}>
          <div style={{ height: 1, width: 50, backgroundColor: "white", marginTop: 30 }} />
          <Col>
            <div style={{ justifyContent: "center", color: "white", fontSize: 30, fontWeight: "bold" }}>
              Login.
              </div>
            <div>
              Hello, let's get started.
              </div>
          </Col>
        </Row>

        <div
          style={{
            // backgroundColor: "#F8F9FB",
            // padding: 10,
            marginTop: 50,
            borderRadius: 5,
            marginRight: 50,
            marginLeft: 50

          }}
        >

          <Row style={{ justifyContent: "center", alignContent: "center" ,marginBottom:50}}>
            <div style={{ height: 1, width: 100, backgroundColor: "white", marginTop: 30 }} />

            <img src="https://dpi.wi.gov/sites/default/files/imce/wiselearn/logos/Google_-G-_Logo.svg.png" width="50" height="50" style={{margin:10}} />
            <img src="https://carlisletheacarlisletheatre.org/images/linkedin-icon-circular-6.png" width="50" height="50" style={{margin:10}}/>
            <img src="https://cdn4.iconfinder.com/data/icons/social-media-icons-the-circle-set/48/facebook_circle-512.png" width="50" height="50" style={{margin:10}}/>

            <div style={{ height: 1, width: 100, backgroundColor: "white", marginTop: 30 }} />

          </Row>
          <TextField
            // id="standard-name"
            label="Your email"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            margin="normal"
            fullWidth
          />
          <TextField
            // id="standard-name"
            label="Password"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            type="password"
            margin="normal"
            fullWidth
          />
          <div style={{ height: 30 }} />
        </div>
        <Button
          type="submit"
          style={{
            borderRadius: 5,
            backgroundColor: "#243D51",
            marginTop: -20,
            //marginLeft: 10,
            alignSelf: "flex_right",
            flex: 1,
            width: 130
          }}
        >
          <div style={{ color: "white", fontSize: 16, textTransform: "none" }}>
            Transfer
          </div>
        </Button>
      </Grid>
    );
  }


  rightImage() {
    return (
      <Grid item xs={12} md={12}>
        <Image
          src={
            "https://trello-attachments.s3.amazonaws.com/5dab01e9df6f8b805e3bea4e/599x1080/dbef486a7384d64d4b4721b669f21631/bg00.jpg"
          }
          style={{ width: "100%", height: "auto" }}
        />
      </Grid>
    );
  }

  render() {
    return (
      <div
        style={{ backgroundColor: "#fff", paddingLeft: 50, paddingRight: 50 }}
      >
        <React.Fragment>
          <CssBaseline />
          <Grid container spacing={4} justify="space-evenly">
            <Grid item xs={12} md={7} style={{ marginTop: 20 }}>
              {this.appBar()}
              <Row style={{ marginTop: 50, marginLeft: -60 }}>
                {this.registerForm()}
              </Row>
            </Grid>

            <Grid item xs={12} md={5} style={{ backgroundColor: "#61C8CF", marginRight: -50 }}>
              {this.loginForm()}
            </Grid>
          </Grid>
        </React.Fragment>
      </div>
    );
  }
}

const styles = {
  appBar: {
    borderBottom: `3px`
  },
  toolbar: {
    flexWrap: "wrap"
  },
  toolbarTitle: {
    flexGrow: 1
  },
  link: {
    margin: 10
  },
  heroContent: {
    padding: 8
  },
  cardHeader: {
    backgroundColor: "grey"
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: 4
  },
  container: {
    // borderTop: `1px solid`,
    //backgroundColor: "grey"
  },
  card: {
    maxWidth: 400,
    borderRadius: 30,
    padding: 30
  },
  cardApps: {
    maxWidth: 700,
    borderRadius: 30,
    marginTop: 20,
    marginBottom: 20,
    padding: 20
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  bigAvatar: {
    marginBottom: -40,
    marginLeft: 160,
    width: 80,
    height: 80,
    elevation: 12
  }
};
