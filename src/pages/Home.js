import React, { Component } from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  TextField,
  CardHeader,
  IconButton,
  CssBaseline,
  Avatar,
  CircularProgress,
  LinearProgress,
  Icon,
  Divider
} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/StarBorder";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { withRouter, Link, Redirect } from 'react-router-dom';
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Slide from "@material-ui/core/Slide";
import Grow from "@material-ui/core/Grow";
import { Row, Col, Image } from "react-bootstrap";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";

import { DropzoneDialog } from "material-ui-dropzone";
import { fontFamily } from "@material-ui/system";

//import bg00 from '../assets/bgOO.jpg';

export default class Home extends Component {
  appBar() {
    return (
      <Grid item xs={12} md={12}>
        <Row>
          <Col>
            <Image
              src={
                "https://trello-attachments.s3.amazonaws.com/5daaf63d70ab311cffed9a03/5dab01c34d6a05454eb475cd/c08bf0cfc4d99495ccbdc2a611b440cd/We.svg"
              }
              style={{ width: 80, height: "auto" }}
            />
          </Col>
          <div
            style={{
              justifyContent: "center",
              color: "grey",
              fontSize: 16,
              marginLeft: 20
            }}
          >
            Contests
          </div>
          <div
            style={{
              justifyContent: "center",
              color: "grey",
              fontSize: 16,
              marginLeft: 20
            }}
          >
            Upgrade
          </div>
          <div
            style={{
              justifyContent: "center",
              color: "grey",
              fontSize: 16,
              marginLeft: 20
            }}
          >
            Help
          </div>
          <Link to={{
             pathname: "/login",
            //  data: 'data' // your data array of objects
          }}>
            <div
              style={{
                justifyContent: "center",
                color: "#61c8d0",
                fontSize: 16,
                marginLeft: 20
              }}
            >
              Sign in
          </div>
          </Link>
        </Row>
      </Grid>
    );
  }

  freeForm() {
    return (
      <Grid item xs={12} md={5} style={{ padding: 10 }}>
        <Row>
          <Col
            style={{ justifyContent: "center", color: "grey", fontSize: 17 }}
          >
            Transferer vos fichiers volumineux gratuitement.
          </Col>
        </Row>
        <div style={{ height: 2, width: 100, backgroundColor: "grey" }} />
        <div
          style={{
            backgroundColor: "#F8F9FB",
            padding: 10,
            marginTop: 30,
            borderRadius: 5
          }}
        >
          <Row style={{}}>
            <Col xs={3}>
              <IconButton
                type="submit"
                style={{ borderRadius: 100, backgroundColor: "#61C8CF" }}
              >
                <Icon style={{ color: "white", fontSize: 30 }}>add</Icon>
              </IconButton>
            </Col>
            <Col style={{ alignSelf: "center" }}>
              <div style={{ justifyContent: "center", fontSize: 20 }}>
                Add your files
              </div>
              <div
                style={{
                  justifyContent: "center",
                  color: "grey",
                  fontSize: 10
                }}
              >
                Or select a folder
              </div>
            </Col>
          </Row>
          <Divider style={{ marginTop: 15 }} />

          <TextField
            // id="standard-name"
            label="Email to"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            margin="normal"
            fullWidth
          />
          <TextField
            // id="standard-name"
            label="Your email"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            margin="normal"
            fullWidth
          />
          <TextField
            // id="standard-name"
            label="Title/Subject"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            margin="normal"
            fullWidth
          />
          <TextField
            // id="standard-name"
            label="Message"
            //className={classes.textField}
            //value={values.name}
            //onChange={handleChange('name')}
            multiline="true"
            rows="4"
            margin="normal"
            fullWidth
          />
          <div style={{ height: 30 }} />
        </div>
        <Button
          type="submit"
          style={{
            borderRadius: 5,
            backgroundColor: "#243D51",
            marginTop: -20,
            marginLeft: 10,
            width: 130
          }}
        >
          <div style={{ color: "white", fontSize: 16, textTransform: "none" }}>
            Transfer
          </div>
        </Button>
      </Grid>
    );
  }

  userForm() {
    return (
      <Grid item xs={12} md={7} style={{ padding: 10 }}>
        <Row>
          <Col
            style={{
              justifyContent: "center",
              color: "grey",
              fontSize: 17,
              textAlign: "right"
            }}
          >
            Faite-vous payer, gérer vos dépenses et protéger votre activité.
          </Col>
        </Row>
        <div
          style={{
            height: 2,
            width: 100,
            backgroundColor: "grey",
            float: "right"
          }}
        />

        <div
          style={{
            backgroundColor: "#F8F9FB",
            padding: 15,
            marginTop: 30,
            borderRadius: 5
          }}
        >
          <Row style={{ justifyContent: "center", alignItems: "center" }}>
            <Col xs={2}>
              <IconButton
                type="submit"
                style={{ borderRadius: 100, backgroundColor: "#61C8CF" }}
              >
                <Icon style={{ color: "white", fontSize: 30 }}>add</Icon>
              </IconButton>
            </Col>
            <Col style={{ alignSelf: "center" }}>
              <div style={{ justifyContent: "center", fontSize: 20 }}>
                Ajoutez vos fichiers
              </div>
              <div
                style={{
                  justifyContent: "center",
                  color: "grey",
                  fontSize: 10
                }}
              >
                Ou glisser vos fichiers sur cette zone.
              </div>
            </Col>
            <Col xs={3} style={{ alignSelf: "center" }}>
              <TextField
                style={{ color: "#61c8d0" }}
                // id="standard-name"
                label="Le prix"
                //className={classes.textField}
                //value={values.name}
                //onChange={handleChange('name')}
                margin="normal"
                fullWidth
              />
            </Col>
          </Row>
          <Divider style={{ marginTop: 20 }} />

          <Row style={{ marginLeft: -17 }}>
            <Col>
              <h3 style={{ color: "#253d51" }}>Freelancer </h3>
              <p style={{ color: "#253d51", fontSize: 12 }}>
                Informations sur l’envoyeur, le propriéétaire des fichiers
                transféré.
              </p>
            </Col>

            <Col style={{ textAlign: "right" }}>
              <h3 style={{ color: "#253d51" }}>Votre Client</h3>
              <p style={{ color: "#253d51", fontSize: 12 }}>
                Celui qui est concerner à payer pour avoir la possibilité de
                télécharger ces fichiers.{" "}
              </p>
            </Col>
          </Row>

          <Row>
            <Grid item xs={12} md={6} style={{ padding: 10 }}>
              <TextField
                // id="standard-name"
                label="Adresse e-mail"
                //className={classes.textField}
                //value={values.name}
                //onChange={handleChange('name')}
                margin="normal"
                fullWidth
              />
              <TextField
                // id="standard-name"
                label="Objet / Titre / Nom du projet"
                //className={classes.textField}
                //value={values.name}
                //onChange={handleChange('name')}
                margin="normal"
                fullWidth
              />

              <TextField
                // id="standard-name"
                label="Message"
                //className={classes.textField}
                //value={values.name}
                //onChange={handleChange('name')}
                multiline="true"
                rows="4"
                margin="normal"
                fullWidth
              />
            </Grid>

            <Grid item xs={12} md={6} style={{ padding: 10 }}>
              <TextField
                // id="standard-name"
                label="Adresse e-mail de votre client "
                //className={classes.textField}
                //value={values.name}
                //onChange={handleChange('name')}
                margin="normal"
                fullWidth
              />
              <TextField
                // id="standard-name"
                label="Category"
                //className={classes.textField}
                //value={values.name}
                //onChange={handleChange('name')}
                margin="normal"
                fullWidth
              />
            </Grid>
          </Row>
          <div style={{ height: 30 }} />
        </div>
        <Row style={{}}>
          <Col>
            <Button
              type="submit"
              style={{
                borderRadius: 5,
                backgroundColor: "#61C8CF",
                marginTop: -20,
                marginLeft: 10,
                width: 130
              }}
            >
              <div
                style={{ color: "white", fontSize: 16, textTransform: "none" }}
              >
                S'inscrire
              </div>
            </Button>
          </Col>
          <Col style={{ alignSelf: "center" }}>
            <Button
              type="submit"
              style={{
                borderRadius: 5,
                backgroundColor: "#243D51",
                marginTop: -20,
                marginLeft: 10,
                width: 130
              }}
            >
              <div
                style={{ color: "white", fontSize: 16, textTransform: "none" }}
              >
                Transférer
              </div>
            </Button>
          </Col>
        </Row>
      </Grid>
    );
  }

  rightImage() {
    return (
      <Grid item xs={12} md={12}>
        <Image
          src={
            "https://trello-attachments.s3.amazonaws.com/5dab01e9df6f8b805e3bea4e/599x1080/dbef486a7384d64d4b4721b669f21631/bg00.jpg"
          }
          style={{ width: "100%", height: "auto" }}
        />
      </Grid>
    );
  }

  render() {
    return (
      <div
        style={{ backgroundColor: "#fff", paddingLeft: 50, paddingRight: 50 }}
      >
        <React.Fragment>
          <CssBaseline />
          <Grid container spacing={4} justify="space-evenly">
            <Grid item xs={12} md={7} style={{ marginTop: 20 }}>
              {this.appBar()}
              <Row style={{ marginTop: 50 }}>
                {this.freeForm()}
                {this.userForm()}
              </Row>
            </Grid>

            <Grid item xs={12} md={4}>
              {this.rightImage()}
            </Grid>

            <Grid item xs={12} md={1} style={{ maxWidth: 20 }}>
              <Icon style={{ fontSize: 30 }}>menu</Icon>
            </Grid>
          </Grid>
        </React.Fragment>
      </div>
    );
  }
}

const styles = {
  appBar: {
    borderBottom: `3px`
  },
  toolbar: {
    flexWrap: "wrap"
  },
  toolbarTitle: {
    flexGrow: 1
  },
  link: {
    margin: 10
  },
  heroContent: {
    padding: 8
  },
  cardHeader: {
    backgroundColor: "grey"
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: 4
  },
  container: {
    // borderTop: `1px solid`,
    //backgroundColor: "grey"
  },
  card: {
    maxWidth: 400,
    borderRadius: 30,
    padding: 30
  },
  cardApps: {
    maxWidth: 700,
    borderRadius: 30,
    marginTop: 20,
    marginBottom: 20,
    padding: 20
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  bigAvatar: {
    marginBottom: -40,
    marginLeft: 160,
    width: 80,
    height: 80,
    elevation: 12
  }
};
